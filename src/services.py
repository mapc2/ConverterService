import os

import requests
from tablepyxl import tablepyxl


def get_html_table(url):
    response = requests.get(url, allow_redirects=True)
    filename = 'print_xlsx'
    with open(f"tmp/{filename}.html", 'wb') as file:
        file.write(response.content)
    return f"tmp/{filename}.html"


def html_to_excel(path):
    output_file = path.replace('.html', '.xlsx')

    try:
        with open(f'{path}', 'r', encoding='utf-8') as file:
            html = file.read()
    except UnicodeDecodeError:
        with open(f'{path}', 'r', encoding='windows-1251') as file:
            html = file.read()

    tablepyxl.document_to_xl(html, output_file)

    return output_file


def delete_file(path):
    os.remove(path)
