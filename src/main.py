from fastapi import FastAPI, BackgroundTasks
from fastapi.responses import FileResponse
from fastapi.requests import Request

from services import get_html_table, html_to_excel, delete_file

app = FastAPI()


@app.get("/")
async def read_item(request: Request, url: str, background_tasks: BackgroundTasks):
    # костыль с передачей параметров
    url = str(request.url).split('?url=')[-1]
    path = get_html_table(url)
    output_path = html_to_excel(path)
    headers = {
        'Content-Disposition': f'attachment; filename="{output_path.split("""/""")[-1]}"'
    }
    background_tasks.add_task(delete_file, path=path)
    background_tasks.add_task(delete_file, path=output_path)
    return FileResponse(f'{output_path}', media_type="application/vnd.ms-excel", headers=headers)
